package dev.rehm.util;

import dev.rehm.models.Department;
import dev.rehm.models.MarketItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory(){
        if(sessionFactory==null){
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
            //"jdbc:sqlserver://crehm-java-azure.database.windows.net:1433;databaseName=training-db"
            settings.put(Environment.URL, System.getenv("DB_URL"));
            //"crehm@crehm-java-azure"
            settings.put(Environment.USER, System.getenv("DB_USER"));
            settings.put(Environment.PASS, System.getenv("DB_PASS"));

            //https://docs.microsoft.com/en-us/sql/connect/jdbc/working-with-a-connection?view=sql-server-ver15
            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //https://www.javatpoint.com/dialects-in-hibernate
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "update"); //create here created our tables
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            //provide hibernate mappings to configuration
            configuration.addAnnotatedClass(MarketItem.class);
            configuration.addAnnotatedClass(Department.class);

            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession(){
        return getSessionFactory().openSession();
    }

}
