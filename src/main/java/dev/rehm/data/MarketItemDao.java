package dev.rehm.data;

import dev.rehm.models.MarketItem;

import java.util.List;

public interface MarketItemDao {

    List<MarketItem> getAllItems();
    MarketItem getItemById(int id);
    List<MarketItem> getItemsInRange(double max, double min);
    List<MarketItem> getItemsWithMaxPrice(double max);
    List<MarketItem> getItemsWithMinPrice(double min);
    MarketItem addNewItem(MarketItem item);
    void deleteItem(int id);

}
