package dev.rehm.data;

import dev.rehm.models.MarketItem;
import dev.rehm.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class MarketItemDaoHibImpl implements MarketItemDao{


    private final Logger logger = LoggerFactory.getLogger(MarketItemDaoHibImpl.class);

    @Override
    public List<MarketItem> getAllItems() {
        try(Session s = HibernateUtil.getSession()){
            // queryString here is the HQL (hibernate query language) equivalent of 'select * from market_item'
            List<MarketItem> items = s.createQuery("from MarketItem", MarketItem.class).list();
            return items;
        }
    }

    @Override
    public MarketItem getItemById(int id) {
        try(Session s = HibernateUtil.getSession()){
//            MarketItem item = s.load(MarketItem.class,id);
            MarketItem item = s.get(MarketItem.class, id);
//            logger.info("got item from the database" + item);
            return item;
        }
    }

    @Override
    public List<MarketItem> getItemsInRange(double min, double max) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<MarketItem> cq = cb.createQuery(MarketItem.class);

            Root<MarketItem> root = cq.from(MarketItem.class);
            cq.select(root);

            cq.where(cb.between(root.get("price"), min, max));

            Query<MarketItem> query = s.createQuery(cq);
            return query.list();
        }
    }

    @Override
    public List<MarketItem> getItemsWithMaxPrice(double max) {
        try(Session s = HibernateUtil.getSession()){
            Query<MarketItem> itemQuery = s.createQuery("from MarketItem where price < :max", MarketItem.class);
//            s.createNamedQuery("myQuery");
            itemQuery.setParameter("max",max);
            return itemQuery.list();
        }
    }

    @Override
    public List<MarketItem> getItemsWithMinPrice(double min) {
        try(Session s = HibernateUtil.getSession()){
            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<MarketItem> cq = cb.createQuery(MarketItem.class);

            Root<MarketItem> root = cq.from(MarketItem.class);
            cq.select(root);

            cq.where(cb.greaterThan(root.get("price"), min));

            Query<MarketItem> query = s.createQuery(cq);
            return query.list();
        }
    }

    @Override
    public MarketItem addNewItem(MarketItem item) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
//            s.persist(item);
            int id = (int) s.save(item);
            item.setId(id);
            logger.info("added new item with id:"+id);
            tx.commit();
            return item;
        }
    }

    @Override
    public void deleteItem(int id) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(new MarketItem(id));
            tx.commit();
        }
    }
}
