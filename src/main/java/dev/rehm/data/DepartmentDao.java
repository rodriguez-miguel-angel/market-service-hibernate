package dev.rehm.data;

import dev.rehm.models.Department;

import java.util.List;

public interface DepartmentDao {

    public List<Department> getAllDepartments();
    public Department createDepartment(Department newDepartment);
}
