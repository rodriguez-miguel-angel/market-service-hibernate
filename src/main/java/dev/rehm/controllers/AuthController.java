package dev.rehm.controllers;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    public void authenticateLogin(Context ctx){
        String user = ctx.formParam("username");  //(Content-Type: application/x-www-form-urlencoded)
        String pass = ctx.formParam("password");
        logger.info("{} attempted login", user);
        if(user!=null && user.equals("crehm@gmail.com")){
            if(pass!=null && pass.equals("supersecret")){
                logger.info("successful login");
                // send back token
                ctx.header("Authorization", "admin-auth-token");
                ctx.status(200);
                return;
            }
            throw new UnauthorizedResponse("Incorrect password");

        }
        // invoke service method checking for user
            // if I get back a user send a token with user ID and user role
            // else throw Unauthorized
        throw new UnauthorizedResponse("Username not recognized");
        }

    public void authorizeToken(Context ctx){
        logger.info("attempting to authorize token");

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String authHeader = ctx.header("Authorization");
        if(authHeader!=null && authHeader.equals("admin-auth-token")){
            logger.info("request is authorized, proceeding to handler method");
        } else {
            logger.warn("improper authorization");
            throw new UnauthorizedResponse();
        }
    }


}
