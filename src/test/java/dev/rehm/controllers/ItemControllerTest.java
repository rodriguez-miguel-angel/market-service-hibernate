package dev.rehm.controllers;

import dev.rehm.models.MarketItem;
import dev.rehm.services.MarketItemService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class ItemControllerTest {

    @InjectMocks
    private ItemController itemController;

    /*
    We created a mock service here, a dummy object which we can give behavior by stubbing its methods
    We could also create a spy, which is a real object whose methods can be stubbed and invoked instead of the real methods
     */
    @Mock
    private MarketItemService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllItemsHandler(){
        Context context = mock(Context.class);

        List<MarketItem> items = new ArrayList<>();
        items.add(new MarketItem(5,3.0, "energy bars"));
        items.add(new MarketItem(7,30.0, "camping stove"));
        items.add(new MarketItem(8,100, "tent"));

        when(service.getAll()).thenReturn(items);
        itemController.handleGetItemsRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRangeWithMin(){
        Context context = mock(Context.class);

        List<MarketItem> items = new ArrayList<>();
        items.add(new MarketItem(7,30.0, "camping stove"));
        items.add(new MarketItem(8,100, "tent"));

        when(service.getItemsInRange("20", null)).thenReturn(items);
        when(context.queryParam("min-price")).thenReturn("20");

        itemController.handleGetItemsRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRangeWithMax(){
        Context context = mock(Context.class);

        List<MarketItem> items = new ArrayList<>();
        items.add(new MarketItem(5,3.0, "energy bars"));
        items.add(new MarketItem(7,30.0, "camping stove"));

        when(service.getItemsInRange(null, "80")).thenReturn(items);
        when(context.queryParam("max-price")).thenReturn("80");

        itemController.handleGetItemsRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRange(){
        Context context = mock(Context.class);

        List<MarketItem> items = new ArrayList<>();
        items.add(new MarketItem(7,30.0, "camping stove"));

        when(service.getItemsInRange("20", "50")).thenReturn(items);
        when(context.queryParam("min-price")).thenReturn("20");
        when(context.queryParam("max-price")).thenReturn("50");

        itemController.handleGetItemsRequest(context);
        verify(context).json(items);
    }


}
