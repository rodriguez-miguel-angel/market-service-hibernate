FROM java:8
COPY build/libs/market-server-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar market-server-1.0-SNAPSHOT.jar